﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public float dampTime = 0.15f;
    public Transform target;

    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        if (target)
        {
            Vector3 from = transform.position;
            Vector3 to = target.position;
            to.z = transform.position.z;

            transform.position -= (from - to) * dampTime * Time.deltaTime;
        }

        //Zoom Begin
        /*float zoom = Input.GetAxis("Mouse ScrollWheel");

        if ((zoom < 0)&&(Camera.main.orthographicSize < 2))
        {
            Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize + 0.1f, Camera.main.orthographicSize);
        }

        if ((zoom > 0) && (Camera.main.orthographicSize > 0.5))
        {
            Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 0.1f, Camera.main.orthographicSize);
        }*/
        //Zoom End

    }

}
