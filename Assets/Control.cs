﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour
{
    public float maxSpeed = 5f;
    public CircleCollider2D burstColl;
    public int burstMax = 1;
    private int burstLeft;
    public float burstSpeed = 0.005f;
    public float burstRMax = 1f;
    private bool bursted = false;
    public Transform bubble;
    public Transform bubble2nd;
    private bool burstComplete = false;
    public float burstFade = 2.0f;
    public float burstCool = 2.0f;
    private bool burstReady = true;
    public Animator Anim;

    // Use this for initialization
    void Start()
    {
        burstLeft = burstMax;
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(0);
            ScoreScript.yourScore = 0;
            ScoreScript.enemyScore = 0;
            ScoreScript.neutralScore = SceneInitialize.SnumOfNpcs;
        }

        if (bursted == true)
        {
            if (bubble.transform.localScale.x < burstRMax) {
                bubble.transform.localScale += new Vector3(burstSpeed * 10, burstSpeed * 10, 0);
                }
            bubble2nd.transform.localScale += new Vector3(burstSpeed * 10, burstSpeed * 10, 0);

            StartCoroutine(afterBurst(burstFade));
        }

        if (burstComplete == true)
        {
            bubble.transform.localScale = new Vector3(1, 1, 1);
            bubble.gameObject.SetActive(false);
            bubble2nd.transform.localScale = new Vector3(1, 1, 1);
            bubble2nd.gameObject.SetActive(false);
            burstComplete = false;
            bursted = false;
        }
    }

    void FixedUpdate()
    {
        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");

        GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * maxSpeed, moveY * maxSpeed);

        if ((Input.GetKeyDown(KeyCode.Space)) && (burstLeft > 0) && (burstReady == true))
        {
            bubble.gameObject.SetActive(true);
            bubble2nd.gameObject.SetActive(true);
            bursted = true;

            StartCoroutine(burstStopper(burstCool));
            burstReady = false;
            burstLeft--;
        }  

        //end Game
        if((ScoreScript.neutralScore < 1) && (burstLeft < 1))
        {
            StartCoroutine(endGame(5));
        }
    }

    IEnumerator afterBurst(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        burstComplete = true;
    }

    IEnumerator burstStopper(float burstCool)
    {
        yield return new WaitForSeconds(burstCool);
        burstReady = true;
    }

    IEnumerator endGame(float waiting)
    {
        yield return new WaitForSeconds(waiting);
        
        if(ScoreScript.yourScore > ScoreScript.enemyScore)
        {
            Application.LoadLevel(1);
        }

        if(ScoreScript.yourScore < ScoreScript.enemyScore)
        {
            Application.LoadLevel(2);
        }
    }

    void OnTriggerEnter2d(Collider2D coll)
    {
        
    }
}
