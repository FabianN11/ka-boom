﻿using UnityEngine;
using System.Collections;

public class SceneInitialize : MonoBehaviour {
    public Transform myPrefab;
    public Transform BContainer;

    public int numOfNpcs;
    static public int SnumOfNpcs;
    private Transform[] Npcs;
    // Use this for initialization
    void Start()
    {
        SnumOfNpcs = numOfNpcs;
        ScoreScript.neutralScore = numOfNpcs;
        Npcs = new Transform[numOfNpcs];

        //create Npcs
        for (int i = 0; i < Npcs.Length; i++)
        {

            //create a random vector on the ground
            float x = UnityEngine.Random.Range(-4.4f, 4.4f);
            float y = UnityEngine.Random.Range(-1.6f, 1.6f);
            float z = 0f;

            Vector3 vec = new Vector3(x, y, z);

            //test for clash
            bool clash = false;
            for (int j = 0; j < i; j++)
            {
                //print (i + ", " +j);
                Vector3 diff = Npcs[j].transform.position - vec;
                if (diff.magnitude < 0.3f)
                {
                    clash = true;
                }
            }

            //create an instance
            if (!clash)
            {
                Npcs[i] = (Transform)Instantiate(myPrefab, vec, Quaternion.identity);
                Npcs[i].transform.parent = BContainer;
            }
            else
            {
                i--;
            }

        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
