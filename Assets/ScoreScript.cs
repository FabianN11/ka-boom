﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour {
    public Text yourScoreField;
    static public int yourScore = 0;
    public Text enemyScoreField;
    static public int enemyScore = 0;
    public Text neutralScoreField;
    static public int neutralScore = 0;

    // Use this for initialization
    void Start () {
        yourScoreField.text = "cured: " + yourScore;
        neutralScoreField.text = "neutral: " + neutralScore;
        enemyScoreField.text = "infected: " + enemyScore;
    }

    // Update is called once per frames
    void FixedUpdate () {
		yourScoreField.text = "cured: " + yourScore;
		neutralScoreField.text = "neutral: " + neutralScore;
		enemyScoreField.text = "infected: " + enemyScore;
    }
}
