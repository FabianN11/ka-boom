﻿using UnityEngine;
using System.Collections;

public class NPCBehaviour : MonoBehaviour {
    private float tChange = 0; // force new direction in the first Update
    private float randomX;
    private float randomY;
    public float moveSpeed = 1.0f;
    public Sprite safe;
    public Sprite cured;

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update() {
        if (Time.time >= tChange)
        {
            randomX = Random.Range(-2.0f, 2.0f); // with float parameters, a random float
            randomY = Random.Range(-2.0f, 2.0f); //  between -2.0 and 2.0 is returned
                                               // set a random interval between 0.5 and 1.5
            tChange = Time.time + Random.Range(0.5f, 1.5f);
        }
        transform.Translate(new Vector3(randomX, randomY, 0) * moveSpeed * Time.deltaTime);
    }

    void FixedUpdate() { }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Burst")
        {

            if (this.tag == "Neutral")
            {
                ScoreScript.neutralScore--;
                ScoreScript.yourScore++;
            }

            if (this.tag == "Infected")
            {
                ScoreScript.enemyScore--;
                ScoreScript.yourScore++;
            }

            this.tag = "Cured";
            this.GetComponent<SpriteRenderer>().sprite = cured;
            this.GetComponent<SpriteRenderer>().color = Color.white;

        }

        if (coll.tag == "Player")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "Neutral")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "Cured")
        {
            randomX *= -1;
            randomY *= -1;

            if (this.tag == "Neutral")
            {
                this.tag = "Cured";
                ScoreScript.yourScore++;
                ScoreScript.neutralScore--;
                this.GetComponent<SpriteRenderer>().sprite = cured;
            }
        }

        if (coll.tag == "HWall")
        {
            randomX *= -1;
        }

        if (coll.tag == "VWall")
        {
            randomY *= -1;
        }

        if (coll.tag == "Infector")
        {
            randomX *= -1;
            randomY *= -1;

            if (this.tag == "Neutral")
            {
                this.tag = "Infected";
                ScoreScript.enemyScore++;
                ScoreScript.neutralScore--;

                this.GetComponent<SpriteRenderer>().sprite = safe;
				this.GetComponent<SpriteRenderer>().color = Color.red;
            }
        }

        if (coll.tag == "Infected")
        {
            randomX *= -1;
            randomY *= -1;

            if (this.tag == "Neutral")
            {
                this.tag = "Infected";
                ScoreScript.enemyScore++;
                ScoreScript.neutralScore--;

                this.GetComponent<SpriteRenderer>().sprite = safe;
                this.GetComponent<SpriteRenderer>().color = Color.red;
            }
        }

        if (coll.tag == "Takeout")
        {
            randomX *= -1;
            randomY *= -1;
        }
    }
}
