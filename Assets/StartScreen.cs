﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour
{
    public Transform Screen;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(on(1));
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Application.LoadLevel(3);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    IEnumerator on(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Screen.gameObject.SetActive(true);
        StartCoroutine(off(1));
    }

    IEnumerator off(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Screen.gameObject.SetActive(false);
        StartCoroutine(on(1));
    }
}
