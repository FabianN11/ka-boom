﻿using UnityEngine;
using System.Collections;

public class PressSpace : MonoBehaviour
{
    public Transform Screen;

    // Use this for initialization
    void Start()
    {
        ScoreScript.yourScore = 0;
        ScoreScript.enemyScore = 0;
        ScoreScript.neutralScore = SceneInitialize.SnumOfNpcs;

        StartCoroutine(toTitle(5));
        StartCoroutine(on(1));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.LoadLevel(3);
        }

    }
    IEnumerator toTitle(float waiting)
    {
        yield return new WaitForSeconds(waiting);
        Application.LoadLevel(0);
    }

    IEnumerator on(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Screen.gameObject.SetActive(true);
        StartCoroutine(off(1));
    }

    IEnumerator off(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Screen.gameObject.SetActive(false);
        StartCoroutine(on(1));
    }
}