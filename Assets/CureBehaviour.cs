﻿using UnityEngine;
using System.Collections;

public class CureBehaviour : MonoBehaviour {
    private float tChange = 0; // force new direction in the first Update
    private float randomX;
    private float randomY;
    public float moveSpeed = 1.0f;
    public Sprite infector;
    public Sprite cured;
    public float infectionTime = 5.0f;

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= tChange)
        {
            randomX = Random.Range(-2.0f, 2.0f); // with float parameters, a random float
            randomY = Random.Range(-2.0f, 2.0f); //  between -2.0 and 2.0 is returned
                                                 // set a random interval between 0.5 and 1.5
            tChange = Time.time + Random.Range(0.5f, 1.5f);
        }
        transform.Translate(new Vector3(randomX, randomY, 0) * moveSpeed * Time.deltaTime);
    }

    void FixedUpdate() { }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Burst")
        {
            this.tag = "Takeout";

            this.GetComponent<SpriteRenderer>().sprite = cured;
            StartCoroutine(regeneration(infectionTime));
        }

        if (coll.tag == "Player")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "Neutral")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "Infected")
        {
            randomX *= -1;
            randomY *= -1;
        }

        if (coll.tag == "HWall")
        {
            randomX *= -1;
        }

        if (coll.tag == "VWall")
        {
            randomY *= -1;
        }

        if (coll.tag == "Infector")
        {
            randomX *= -1;
            randomY *= -1;

            this.tag = "Infector";

            this.GetComponent<SpriteRenderer>().sprite = infector;
        }

        if (coll.tag == "Cured")
        {
            randomX *= -1;
            randomY *= -1;
        }
    }

    IEnumerator regeneration(float infectionTime)
    {
        yield return new WaitForSeconds(infectionTime);
        this.tag = "Infector";

        this.GetComponent<SpriteRenderer>().sprite = cured;
    }
}
